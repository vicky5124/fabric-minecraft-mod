package net.my.mod

import net.fabricmc.api.ModInitializer
import net.fabricmc.fabric.api.block.FabricBlockSettings
import net.fabricmc.fabric.api.tools.FabricToolTags
import net.minecraft.block.Block
import net.minecraft.block.Blocks
import net.minecraft.block.Material
import net.minecraft.item.BlockItem
import net.minecraft.item.Item
import net.minecraft.item.ItemGroup
import net.minecraft.sound.BlockSoundGroup
import net.minecraft.sound.SoundEvent
import net.minecraft.util.Identifier
import net.minecraft.util.registry.BuiltinRegistries
import net.minecraft.util.registry.Registry
import net.minecraft.world.gen.decorator.Decorator
import net.minecraft.world.gen.decorator.RangeDecoratorConfig
import net.minecraft.world.gen.feature.ConfiguredFeature
import net.minecraft.world.gen.feature.Feature
import net.minecraft.world.gen.feature.OreFeatureConfig

// Sounds

val cubeBlockSoundId = Identifier("mymod:hl1_crow_bar")
val cubeBlockSoundEvent = SoundEvent(cubeBlockSoundId)
val cubeBlockSoundGroup = BlockSoundGroup(0.01f, 1.2f, cubeBlockSoundEvent, cubeBlockSoundEvent, cubeBlockSoundEvent, cubeBlockSoundEvent, cubeBlockSoundEvent)

// Items

val cubeIngot = Item(
    Item.Settings().group(ItemGroup.MISC)
)

// Blocks

val cubeBlock = Block(
    FabricBlockSettings
        .of(Material.GLASS)
        .breakByHand(false)
        .breakByTool(FabricToolTags.SHOVELS, 1)
        .hardness(1.0f)
        .build()
        .requiresTool()
        .sounds(cubeBlockSoundGroup)
)

val cubeOre = Block(
    FabricBlockSettings
        .of(Material.STONE)
        .breakByHand(false)
        .breakByTool(FabricToolTags.PICKAXES, 2)
        .hardness(3.2f)
        .build()
        .requiresTool()
)

// Ore Generation

@kotlin.jvm.JvmField
public val cubeOreOverworld: ConfiguredFeature<*, *> = Feature.ORE
    .configure(
        OreFeatureConfig(
            OreFeatureConfig.Rules.BASE_STONE_OVERWORLD,
            cubeOre.defaultState,
            8
        )
    ) // vein size
    .decorate(
        Decorator.RANGE.configure(
            RangeDecoratorConfig(
                0,  // bottom offset
                0,  // min y level
                64
            )
        )
    ) // max y level
    .spreadHorizontally()
    .repeat(8) // number of veins per chunk

fun init() {
    println("Hello Fabric world!")

    // Sounds

    Registry.register(Registry.SOUND_EVENT, cubeBlockSoundId, cubeBlockSoundEvent)

    // Items

    Registry.register(Registry.ITEM, Identifier("mymod", "cube_ingot"), cubeIngot)

    // Blocks

    Registry.register(Registry.BLOCK, Identifier("mymod", "cube_block"), cubeBlock)
    Registry.register(Registry.BLOCK, Identifier("mymod", "cube_ore"), cubeOre)

    Registry.register(
        Registry.ITEM,
        Identifier("mymod", "cube_block"),
        BlockItem(
            cubeBlock,
            Item.Settings().group(ItemGroup.MISC)
        )
    )

    Registry.register(
        Registry.ITEM,
        Identifier("mymod", "cube_ore"),
        BlockItem(
            cubeOre,
            Item.Settings().group(ItemGroup.MISC)
        )
    )

    // Ore World Gen

    Registry.register(
        BuiltinRegistries.CONFIGURED_FEATURE,
        Identifier("mymod", "cube_ore"),
        cubeOreOverworld
    )
}
